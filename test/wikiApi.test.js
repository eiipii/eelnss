import api from '../src/api'
import qunit from './utils/qunitApiMapping'

test('simple len', () => {
    let simple = api.buildLen("simple");
    qunit.propEqual(
        simple.set("anyValue", {}),
        {
            "simple": "anyValue"
        }
    );
    qunit.equal(
        simple.get({"simple": "valueToExtract"}),
        "valueToExtract"
    );
});

test('Really nested a.b.c.d.e.f.g', () => {
    let nested = api.buildLen("a.b.c.d.e.f.g");
    qunit.propEqual(
        nested.set("nestedValueToSet", {}),
        {
            "a": {
                "b": {
                    "c": {
                        "d": {
                            "e": {
                                "f": {
                                    "g": "nestedValueToSet"
                                }
                            }
                        }
                    }
                }
            }
        }
    );
    qunit.equal(
        nested.get(
            {
                "a": {
                    "b": {
                        "c": {
                            "d": {
                                "e": {
                                    "f": {
                                        "g": "nestedValueToExtract"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        ),
        "nestedValueToExtract"
    );
});
test('Parallel lenses as telescopes', () => {
    let telescope = api.buildLen("(x,y,z)");
    qunit.propEqual(
        telescope.set([1, 2, 3], {}),
        {
            "x": 1,
            "y": 2,
            "z": 3
        }
    );
    qunit.propEqual(
        telescope.get({
            "x": "xValue",
            "y": "yValue",
            "z": "zValue"
        }),
        ["xValue", "yValue", "zValue"]
    );
});
test('Lenses for Maps - simple case', () => {
    let dbObject = {
        nestedMap: {
            "key1": "value1",
            "key2": "value2"
        }
    };
    let key1Len = api.buildLen("nestedMap.key1");
    let key2Len = api.buildLen("nestedMap.key2");
    qunit.propEqual(
        key1Len.get(dbObject), "value1"
    );
    qunit.propEqual(
        key2Len.get(dbObject), "value2"
    );
    dbObject = key2Len.set("updatedValue2", dbObject);
    qunit.propEqual(
        dbObject, {
            nestedMap: {
                "key1": "value1",
                "key2": "updatedValue2"
            }
        }
    );
});

test('Lenses for Maps - using clens', () => {
    let dbObject = {
        nestedMap: {
            "key1": "value1",
            "key2": "value2"
        }
    };
    let mapClen = api.buildContextLen("nestedMap.{:key}");
    qunit.propEqual(
        mapClen.cget(["key1"], dbObject), "value1"
    );
    qunit.propEqual(
        mapClen.cget(["key2"], dbObject), "value2"
    );
    dbObject = mapClen.cset(["key2"], "updatedValue2", dbObject);
    qunit.propEqual(
        dbObject, {
            nestedMap: {
                "key1": "value1",
                "key2": "updatedValue2"
            }
        }
    );
});
test('SQL like operations - Insert', () => {
    let usersCLen = api.buildContextLen("app.users.{:uID}.(email,data.name,isActive)");
    let users = [
        ["u1", "admin@pmsoft.eu", "administrator", true],
        ["u2", "normal@pmsoft.eu", "user", true],
        ["u3", "testUser@pmsoft.eu", "testUser", false]
    ];
    let state = {};
    // Equivalent to update (or insert if no present)
    state = usersCLen.lset(users, state);
    qunit.propEqual(
        state,
        {
            "app": {
                "users": {
                    "u1": {
                        "data": {
                            "name": "administrator"
                        },
                        "email": "admin@pmsoft.eu",
                        "isActive": true
                    },
                    "u2": {
                        "data": {
                            "name": "user"
                        },
                        "email": "normal@pmsoft.eu",
                        "isActive": true
                    },
                    "u3": {
                        "data": {
                            "name": "testUser"
                        },
                        "email": "testUser@pmsoft.eu",
                        "isActive": false
                    }
                }
            }
        }
    );
    const contextValues = usersCLen.extract(state);
    expect(contextValues).toStrictEqual([
        ["u1"],
        ["u2"],
        ["u3"]
    ]);
    //Equivalent do select * from state
    let selectUsers = usersCLen.lget(state);
    qunit.propEqual(
        selectUsers,
        [
            ["u1", "admin@pmsoft.eu", "administrator", true],
            ["u2", "normal@pmsoft.eu", "user", true],
            ["u3", "testUser@pmsoft.eu", "testUser", false]
        ]
    );

    //Equivalent to select * from state where email = 'admin@pmsoft.eu'
    //Fileds names are taken from the telescope part of the clen.
    let adminUserSelected = usersCLen.find({
        email: "admin@pmsoft.eu"
    }).on(state);
    qunit.propEqual(
        adminUserSelected,
        [
            ["u1", "admin@pmsoft.eu", "administrator", true]
        ]
    );

    //Equivalent to update/insert where uid='u3'
    state = usersCLen.lset(
        [
            ["u3", "testUser@pmsoft.eu", "testUser", true]
        ],
        state);
    qunit.propEqual(
        state,
        {
            "app": {
                "users": {
                    "u1": {
                        "data": {
                            "name": "administrator"
                        },
                        "email": "admin@pmsoft.eu",
                        "isActive": true
                    },
                    "u2": {
                        "data": {
                            "name": "user"
                        },
                        "email": "normal@pmsoft.eu",
                        "isActive": true
                    },
                    "u3": {
                        "data": {
                            "name": "testUser"
                        },
                        "email": "testUser@pmsoft.eu",
                        "isActive": true
                    }
                }
            }
        }
    );
});


test('a.{:mapId}.(a.b, x.z, complex.nested) mapped to person.{:cid}.(name, contact, c)', () => {
    let simple = api.buildContextLen("a.{:mapId}.(a.b, x.z, complex.nested)");
    let initialData = [
        ["key1", 1, 2, 3],
        ["key2", 1, 2, 3]
    ];
    let setInitial = simple.lset(initialData, {});
    qunit.propEqual(setInitial, {
        "a": {
            "key1": {
                "a": {
                    "b": 1
                },
                "complex": {
                    "nested": 3
                },
                "x": {
                    "z": 2
                }
            },
            "key2": {
                "a": {
                    "b": 1
                },
                "complex": {
                    "nested": 3
                },
                "x": {
                    "z": 2
                }
            }
        }
    }, "Set initial match");

    let target = api.buildContextLen("person.{:cid}.(name, contact, c)");
    let personsSchemat = target.lset(simple.lget(setInitial), {});

    qunit.propEqual(personsSchemat, {
        "person": {
            "key1": {
                "c": 3,
                "contact": 2,
                "name": 1
            },
            "key2": {
                "c": 3,
                "contact": 2,
                "name": 1
            }
        }
    }, "Values in persons schemas");

});
