import api from '../src/api'
import qunit from './utils/qunitApiMapping'

test('Single table CRUD', () => {
    let tableClen = api.buildContextLen("mytable.{:id}.(firstName,secondName)");
    //Initial state
    let state = {};
    // Create operation
    let dataToInsert = [
        ["id1", "Pedro", "Lopez"],
        ["id2", "Speedy", "Gonzalez"],
        ["id3", "Pedro", "Gonzalez"],
        ["id4", "Jan", "Kowalski"]
    ];
    state = tableClen.lset(dataToInsert, state);
    //state after create
    qunit.propEqual(
        state,
        {
            "mytable": {
                "id1": {
                    "firstName": "Pedro",
                    "secondName": "Lopez"
                },
                "id2": {
                    "firstName": "Speedy",
                    "secondName": "Gonzalez"
                },
                "id3": {
                    "firstName": "Pedro",
                    "secondName": "Gonzalez"
                },
                "id4": {
                    "firstName": "Jan",
                    "secondName": "Kowalski"
                }
            }
        }
    );

    //Read by id
    let pedroUser = tableClen.find({
        id: "id1"
    }).on(state);

    qunit.propEqual(
        pedroUser,
        [["id1", "Pedro", "Lopez"]]
    );
    //Read by property
    let gonzalezPersons = tableClen.find({
        secondName: "Gonzalez"
    }).on(state);
    qunit.propEqual(
        gonzalezPersons,
        [
            ["id2", "Speedy", "Gonzalez"],
            ["id3", "Pedro", "Gonzalez"]
        ]
    );

    //Update existing values
    let updateFullInfo = [
        ["id1", "Pedro", "UpdatedSecondName"]
    ];
    state = tableClen.lset(updateFullInfo, state);
    //state after update
    qunit.propEqual(
        state,
        {
            "mytable": {
                "id1": {
                    "firstName": "Pedro",
                    "secondName": "UpdatedSecondName"
                },
                "id2": {
                    "firstName": "Speedy",
                    "secondName": "Gonzalez"
                },
                "id3": {
                    "firstName": "Pedro",
                    "secondName": "Gonzalez"
                },
                "id4": {
                    "firstName": "Jan",
                    "secondName": "Kowalski"
                }
            }
        }
    );

    //Delete a existing value by ID
    let tableClenIdRef = api.buildContextLen("mytable.{:id}");
    let idToDelete = [["id1"], ["id3"]];
    state = tableClenIdRef.lset(idToDelete, state);
    //state after update
    qunit.propEqual(
        state,
        {
            "mytable": {
                "id2": {
                    "firstName": "Speedy",
                    "secondName": "Gonzalez"
                },
                "id4": {
                    "firstName": "Jan",
                    "secondName": "Kowalski"
                }
            }
        }
    );
    //select do not find the deleted elements
    let deletedSelect = tableClen.find({
        id: "id1"
    }).on(state);
    qunit.propEqual(
        deletedSelect,
        []
    );
});
