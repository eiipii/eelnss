import api from '../src/api'
import contextLensesLaws from "./utils/contextLensesLaws";
import qunit from './utils/qunitApiMapping'


test('a X b == (a,b)', () => {
    let aLen = api.buildContextLen("a");
    let bLen = api.buildContextLen("b");
    let abLen = api.buildContextLen("(a,b)");
    let aXb = api.crossProduct(aLen, bLen);

    let setAB = aXb.cset([], [1, 2], {});
    qunit.propEqual(setAB, {a: 1, b: 2}, "cross product set all values");

    contextLensesLaws.checkContextLenApi(aXb);
    contextLensesLaws.checkContextLenEquivalence(aXb, abLen);
});

test('(a,b,c) X d == (a,b,c,d)', () => {
    let aLen = api.buildContextLen("(a,b,c)");
    let bLen = api.buildContextLen("d");
    let abLen = api.buildContextLen("(a,b,c,d)");
    let aXb = api.crossProduct(aLen, bLen);
    contextLensesLaws.checkContextLenApi(aXb);
    contextLensesLaws.checkContextLenEquivalence(aXb, abLen);
});
test('(a,b) X (c,d) == (a,b,c,d)', () => {
    let aLen = api.buildContextLen("(a,b)");
    let bLen = api.buildContextLen("(c,d)");
    let abLen = api.buildContextLen("(a,b,c,d)");
    let aXb = api.crossProduct(aLen, bLen);
    contextLensesLaws.checkContextLenApi(aXb);
    contextLensesLaws.checkContextLenEquivalence(aXb, abLen);
});

test('a.b X c.d == (a.b,c.d)', () => {
    let aLen = api.buildContextLen("a.b");
    let bLen = api.buildContextLen("c.d");
    let abLen = api.buildContextLen("(a.b,c.d)");
    let aXb = api.crossProduct(aLen, bLen);
    contextLensesLaws.checkContextLenApi(aXb);
    contextLensesLaws.checkContextLenEquivalence(aXb, abLen);
});

test('a.b X a.d == (a.b,a.d)', () => {
    let aLen = api.buildContextLen("a.b");
    let bLen = api.buildContextLen("a.d");
    let abLen = api.buildContextLen("(a.b,a.d)");
    let aXb = api.crossProduct(aLen, bLen);
    contextLensesLaws.checkContextLenApi(aXb);
    contextLensesLaws.checkContextLenEquivalence(aXb, abLen);
});

test('a.b X a.d == a.(b,d)', () => {
    let aLen = api.buildContextLen("a.b");
    let bLen = api.buildContextLen("a.d");
    let abLen = api.buildContextLen("a.(b,d)");
    let aXb = api.crossProduct(aLen, bLen);
    contextLensesLaws.checkContextLenApi(aXb);
    contextLensesLaws.checkContextLenEquivalence(aXb, abLen);
});

test('a X b X c X d == (a,b,c,d)', () => {
    let aLen = api.buildContextLen("a");
    let bLen = api.buildContextLen("b");
    let cLen = api.buildContextLen("c");
    let dLen = api.buildContextLen("d");
    let abLen = api.buildContextLen("(a,b,c,d)");
    let aXbXcXd = api.crossProduct(aLen, bLen, cLen, dLen);
    contextLensesLaws.checkContextLenApi(aXbXcXd);
    contextLensesLaws.checkContextLenEquivalence(aXbXcXd, abLen);
});

