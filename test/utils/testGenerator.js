import _ from 'lodash';

// Generate unique string values
let keyCounter = 0;

function keyNameGen() {
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    function letters(end, counter) {
        if (counter < possible.length) {
            return possible.charAt(counter) + end;
        }
        return letters(possible.charAt(counter % possible.length) + end, counter / possible.length);
    }

    return letters("_key", keyCounter++);
}

// Generate a set of [size] unique string values
function generateUniqueKeySet(size) {
    return _.chain(_.range(size)).map(keyNameGen).value();
}

// from a list of sets S_i, create a list of all [v1,v2,...,vn] values where v_i in set S_i
// Result size = Product of Card(S_i)
function cartesianProduct(listOfSets) {
    if (listOfSets.length === 0) {
        return [
            []
        ];
    }
    let head = _.first(listOfSets);
    if (head.length === 0) {
        return [
            []
        ];
    }
    let deeper = cartesianProduct(_.tail(listOfSets));
    return _.chain(head).map(function (a) {
        return _.chain(deeper).map(function (product) {
            return _.flatten([a, product], true);
        }).value();
    }).flatten(true).value();
}

// Generate a list unique values [v_1,...,v_n] where v_i have keysSize different values , and n = contextSize
// Size = nrOfPossibleKeyValues ^ contextSize
// Size of elements = contextSize
function _lensUniqueContextGenerator(nrOfPossibleKeyValues, contextSize) {
    let listOfKeysPerContainer = _.chain(_.range(contextSize)).map(() => {
        return generateUniqueKeySet(nrOfPossibleKeyValues);
    }).value();
    return cartesianProduct(listOfKeysPerContainer);
}

//Extend the context keys with the values to setup
// Size of array = nrOfPossibleKeyValues ^ contextSize
// Size of elements = contextSize + valueSize
function contextLensValuesGenerator(nrOfPossibleKeyValues, contextSize, valueSize) {
    let uniqueContextValues = _lensUniqueContextGenerator(nrOfPossibleKeyValues, contextSize);
    return _.chain(uniqueContextValues).map(function (context) {
        return _.union(context, generateUniqueKeySet(valueSize));
    }).value();
}


// For a list of values [v_1,v_2,...,v_n] change the values v_i where i > contextSize
function mutateValuesOnContext(listOfValues, contextSize, valueSize) {
    return _.chain(listOfValues).map(function (values) {
        return _.take(values, contextSize);
    }).map(function (contextOnly) {
        return _.union(contextOnly, generateUniqueKeySet(valueSize));
    }).value();
}

export default {
    mutateValuesOnContext,
    contextLensValuesGenerator,
    cartesianProduct,
    generateUniqueKeySet,
    keyNameGen
};
