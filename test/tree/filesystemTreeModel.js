const example = {
    inode: 1,
    properties: {
        name: "root"
    },
    content: [
        {
            subfolder: {
                inode: 2,
                properties: {
                    name: "sub"
                },
                content: [
                    {
                        file: {
                            filehash: "456456",
                            properties: {
                                filename: "asdfasdf"
                            }
                        }
                    }
                ]
            }
        },
        {
            file: {
                filehash: "234",
                properties: {
                    filename: "test"
                }
            }
        },
        {
            device: {
                device_id: "XXX",
                properties: {
                    bitmap: "234"
                }
            }
        }
    ]
}

const identity = (c) => c;

const createNodeModel = ({ntype, codeField, childrenField, childrenToTree, treeToChildren}) => ({
    matchNode: (node) => node[codeField] || node.type === ntype,
    nodeModel: {
        getLabel: tree => ({
            type: ntype,
            code: tree[codeField],
            properties: tree.properties
        }),
        getChildren: tree => (tree[childrenField] || []).map(childrenToTree ? childrenToTree : identity),
        constructTree: (label, children) => {
            const tree = {
                properties: label.properties
            };
            tree[codeField] = label.code;
            tree[childrenField] = children.map(treeToChildren ? treeToChildren : identity);
            return tree;
        }
    }
});


const folderNode = createNodeModel({
    ntype: "folder",
    codeField: "inode",
    childrenField: "content",
    childrenToTree: (c) => c.subfolder || c.file || c.device,
    treeToChildren: (t) => {
        if (t.inode) {
            return {
                subfolder: t
            }
        }
        if (t.filehash) {
            return {
                file: t
            }
        }
        //No other option
        // if (t.device_id) {
        return {
            device: t
        }
        // }
    },
});
const fileNodes = createNodeModel({
    ntype: "file",
    codeField: "filehash",
    childrenField: "noChildren"
});
const deviceNode = createNodeModel({
    ntype: "device",
    codeField: "device_id",
    childrenField: "subdevice"
});


const selectNodeType = node => {
    const nodeType = [
        fileNodes,
        deviceNode,
        folderNode,
    ].find(len => len.matchNode(node))
    return nodeType.nodeModel;
}

const treeModel = {
    getLabel: tree => selectNodeType(tree).getLabel(tree),
    getChildren: tree => selectNodeType(tree).getChildren(tree),
    constructTree: (label, children) => selectNodeType(label).constructTree(label, children)
};


export default {
    treeModel,
    example
};
