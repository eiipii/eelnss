import lenses from "./lenses";
import contextLenses from "./contextLenses";
import api from "./api";

import treeISO from "./tree/treeISO";
import foldableTreeModel from "./tree/foldableTreeModel";
import algebraTypes from "./tree/algebraTypes";

export default {
    lenses: lenses,
    contextLenses: contextLenses,
    api: api,
    tree: {
        iso: treeISO,
        aggregate: foldableTreeModel,
        algebra: algebraTypes
    }
};
