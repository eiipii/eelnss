import _ from 'lodash';
import assert from "./assert";
import lenses from "./lenses";
import contextLenses from "./contextLenses";


function buildNestedContextLen(contextLenExpressionRaw, allowContext) {

    function telescopePartBuilder(part, last) {
        assert(last === true, " telescope parts must be at the end of the context len expression");
        const setParams = (/\((.*)\)/gi).exec(part)[1];
        // const telescopeLensDefinitions = setParams.length === 0 ? [] : setParams.split(",");
        const telescopeLensDefinitions = setParams.split(",");

        const listOfLens = _.map(telescopeLensDefinitions, lenses.nestedFieldBuilder);
        return contextLenses.contextLenFromLen(lenses.telescopeCompose.apply({}, listOfLens));
    }

    function fieldPartBuilder(part, last) {
        if (last) {
            return contextLenses.contextLenFromLen(lenses.fieldLenLeaf(part));
        } else {
            return contextLenses.contextLenFromLen(lenses.fieldLen(part));
        }
    }

    function mapPartBuilder(part, last) {
        if (!allowContext) {
            throw new Error("Trying to build a len with a context parameter??")
        }
        let mapcLen;
        if (last) {
            mapcLen = contextLenses.mapContextLenLeaf;
        } else {
            mapcLen = _.clone(contextLenses.mapContextLen);
        }
        mapcLen.pointers = {};
        mapcLen.pointers[part] = mapcLen;
        mapcLen.spec = _.clone(mapcLen.spec);
        mapcLen.spec.selfPointer = part;
        // "{:anyName}" -> "anyName"
        mapcLen.spec.contextMap = [part.substr(2, part.length - 3)];
        mapcLen.spec.signature = part;
        return mapcLen;
    }

    // clear white spaces
    const contextLenExpression = contextLenExpressionRaw.replace(/ /g, '');
    // Extract final telescope expression
    const telescopeSeparator = contextLenExpression.indexOf("(");
    let parts;
    switch (telescopeSeparator) {
        case -1:
            parts = contextLenExpression.split(".");
            break;
        case 0:
            parts = [contextLenExpression];
            break;
        default:
            // expression is a.b.c.(telescope) -> split to a.b.c (telescope)
            const contextParts = contextLenExpression.substring(0, telescopeSeparator - 1);
            const telescopePart = contextLenExpression.substring(telescopeSeparator);
            parts = contextParts.split(".");
            parts.push(telescopePart);
    }
    const cLenInstance = _.chain(parts).map(function (part, index) {
        const type = part.charAt(0);
        const isLast = ((parts.length - index - 1) === 0);
        // Switch to the different part builders.
        switch (type) {
            case '(':
                return telescopePartBuilder(part, isLast);
            case '{':
                return mapPartBuilder(part, isLast);
            default :
                return fieldPartBuilder(part, isLast);
        }
    }).reduce(contextLenses.contextLenComposition, contextLenses.idContextLen).value();
    cLenInstance.signature = contextLenExpression;
    return cLenInstance;
}


function crossProduct() {
    if (arguments.length < 2) {
        throw new Error("Pass at least 2 clens to build a cross product")
    }
    const args = Array.prototype.slice.call(arguments);
    const head = args[0];
    const tail = _.tail(args);
    return _.reduce(tail, crossProductComposition, head);
}

function crossProductComposition(Aclen, Bclen) {
    const crossContextSize = Aclen.spec.contextSize + Bclen.spec.contextSize;
    const crossValueSize = Aclen.spec.valueSize + Bclen.spec.valueSize;
    const crossProductSignature = Aclen.spec.signature + " X " + Bclen.spec.signature;
    const crossProductSpecification = {
        contextSize: crossContextSize,
        valueSize: crossValueSize,
        signature: crossProductSignature,
        pointers: {},
        contextMap: Aclen.spec.contextMap.concat(Bclen.spec.contextMap),
        valueMap: Aclen.spec.valueMap.concat(Bclen.spec.valueMap),
        selfPointer: void (0)
    };

    function crossCget(context, a) {
        const Acontext = context.slice(0, Aclen.spec.contextSize);
        const Bcontext = context.slice(Aclen.spec.contextSize);
        let AValues = Aclen.cget(Acontext, a);
        let BValues = Bclen.cget(Bcontext, a);
        if (Aclen.spec.valueSize === 1) {
            AValues = [AValues];
        }
        if (Bclen.spec.valueSize === 1) {
            BValues = [BValues];
        }
        return AValues.concat(BValues);
    }

    function crossCset(context, c, a) {
        if (_.isUndefined(c)) {
            c = _.range(crossValueSize).map(function () {
                return void (0);
            });
        }
        const Acontext = context.slice(0, Aclen.spec.contextSize);
        const Bcontext = context.slice(Aclen.spec.contextSize);
        let Avalue = c.slice(0, Aclen.spec.valueSize);
        let Bvalue = c.slice(Aclen.spec.valueSize);
        if (Aclen.spec.valueSize === 1) {
            Avalue = Avalue[0];
        }
        if (Bclen.spec.valueSize === 1) {
            Bvalue = Bvalue[0];
        }
        const afterA = Aclen.cset(Acontext, Avalue, a);
        const afterB = Bclen.cset(Bcontext, Bvalue, afterA);
        return afterB;
    }

    function crossExtactor(a) {
        const Acontexts = Aclen.extract(a);
        const Bcontexts = Bclen.extract(a);
        return _.chain(Acontexts).map(function (aContextSingle) {
            return _.chain(Bcontexts).map(function (bContextSingle) {
                return _.flatten([aContextSingle, bContextSingle], true);
            }).value();
        }).flatten(true).value();
    }

    const finalCross = contextLenses.defineContextLen(crossCget, crossCset, crossExtactor, crossProductSpecification);
    finalCross.signature = crossProductSignature;
    return finalCross;
}

function buildLen(lenExpression) {
    const contextLen = buildNestedContextLen(lenExpression, false);
    return contextLen.bindContext([]);
}

function buildContextLen(lenExpression) {
    return buildNestedContextLen(lenExpression, true)
}

export default {
    buildLen,
    buildContextLen,
    crossProduct
};

