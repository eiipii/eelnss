import algebraTypes from "../../src/tree/algebraTypes";

const intMonoid = algebraTypes.monoids.intMonoid;

describe.only('Algebra operations', () => {

    test('Use simple monoids', () => {
        let monoid = algebraTypes.monoids.intMonoid;
        const suma = monoid.operation(1, 2)
        expect(suma).toBe(3);
        const theSame = monoid.operation(suma, monoid.zero())
        expect(theSame).toBe(suma);
    });


    test('clenMappedMonoids must have empty context', () => {
        const t = () => {
            algebraTypes.structures.clenMappedMonoids("db.{:nodeID}.(x,y,z)", [intMonoid, intMonoid, intMonoid])
        };
        expect(t).toThrow(Error);
        expect(t).toThrow("No context expected");
    });

    test('Lenses pointing to monoid value', () => {

        let mX = algebraTypes.structures.lenMappedMonoid("x", intMonoid)
        expect(mX.operation({
            x: 3
        }, {
            x: 7
        })).toStrictEqual({
            x: 10
        });
    });
    test('List monoid example', () => {
        const listMonoid = algebraTypes.monoids.listMonoid;
        let tasksLM = algebraTypes.structures.lenMappedMonoid("tasks", listMonoid)

        expect(tasksLM.zero()).toStrictEqual({tasks: []});

        expect(tasksLM.operation({
            tasks: ["shopping", "publish"]
        }, {
            tasks: ["compile"]
        })).toStrictEqual({
            tasks: ["shopping", "publish", "compile"]
        });
    });

    test('Magma optioned', () => {
        const timeSegmentMonoid = algebraTypes.monoids.timeSegmentMonoid

        const zero = timeSegmentMonoid.zero();
        expect(zero).toBeUndefined();
        const segment1 = timeSegmentMonoid.operation([0, 1], [0, 3])
        expect(segment1).toStrictEqual([0, 3]);

        let onLenMagma = algebraTypes.structures.lenMappedMonoid("segment", timeSegmentMonoid)
        expect(onLenMagma.zero()).toStrictEqual({});
        expect(onLenMagma.operation({
            segment: [0, 4]
        }, {})).toStrictEqual({
            segment: [0, 4]
        });
        expect(onLenMagma.operation({}, {
            segment: [0, 4]
        })).toStrictEqual({
            segment: [0, 4]
        });
        expect(onLenMagma.operation({
            segment: [0, 4]
        }, {
            segment: [3, 8]
        })).toStrictEqual({
            segment: [0, 8]
        });
    });


    test('Cross product as list of monoids', () => {
        const int3x = algebraTypes.constructors.crossProductMonoids([intMonoid, intMonoid, intMonoid])
        expect(int3x.zero()).toStrictEqual([0, 0, 0]);
        expect(int3x.operation([1, 2, 3], [4, 5, 6])).toStrictEqual([5, 7, 9]);
    });

    test('Cross product with clen pointer - no context', () => {

        const coordinatesMonoid = algebraTypes.structures.clenMappedMonoids("(x,y,z)", [intMonoid, intMonoid, intMonoid])
        expect(coordinatesMonoid.zero()).toStrictEqual({
            x: 0, y: 0, z: 0
        });
        expect(coordinatesMonoid.operation(
            {
                x: 1, y: 2, z: 3
            },
            {
                x: 4, y: 5, z: 6
            }
        )).toStrictEqual(
            {
                x: 5, y: 7, z: 9
            }
        );
    });

    test('Cross product with clen pointer with context', () => {
        let nodesWithMonoids = algebraTypes.structures.clenWithContextMonoids("db.{:nodeID}.(x,y,z)", [intMonoid, intMonoid, intMonoid])
            .onArrayContext([
                ["node1"],
                ["node2"],
                ["node3"],
            ]);
        const dbZero = nodesWithMonoids.zero()
        expect(dbZero).toMatchSnapshot()
        const toAdd = {
            "db": {
                "node1": {
                    "x": 3,
                    "y": 5,
                    "z": -1,
                },
                "node2": {
                    "x": 2,
                    "y": 5,
                    "z": 0,
                },
                "node3": {
                    "x": 3,
                    "y": 6,
                    "z": 1,
                }
            },
        }
        const dbOneState = nodesWithMonoids.operation(dbZero, toAdd);
        expect(dbOneState).toMatchSnapshot()

        let nodesWithMonoidsFromObject = algebraTypes.structures.clenWithContextMonoids("db.{:nodeID}.(x,y,z)", [intMonoid, intMonoid, intMonoid])
            .onContext(dbZero);
        const dbTwoState = nodesWithMonoidsFromObject.operation(dbOneState, toAdd);
        expect(dbTwoState).toMatchSnapshot()

    });

});
