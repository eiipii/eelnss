import treeISO from "../../src/tree/treeISO";
import dataLabelTreeModel from "./dataLabelTreeModel";
import filesystemTreeModel from "./filesystemTreeModel";

describe('Tree models isomorphism', () => {

    test('PM to data label isomorphism', () => {
        const dataLabelTree = treeISO.treeIsomorphism(
            {
                sourceModel: filesystemTreeModel.treeModel,
                targetModel: dataLabelTreeModel.treeModel
            }
        ).transform(filesystemTreeModel.example);
        expect(dataLabelTree).toMatchSnapshot();

        const planRecreated = treeISO.treeIsomorphism(
            {
                sourceModel: dataLabelTreeModel.treeModel,
                targetModel: filesystemTreeModel.treeModel
            }
        ).transform(dataLabelTree);
        expect(planRecreated).toMatchSnapshot();
    });


});
