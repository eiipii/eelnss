import api from '../src/api'
import qunit from './utils/qunitApiMapping'

test('Boolean state change', () => {
    let loggedLen = api.buildContextLen("app.logged");
    let state = {};
    state = loggedLen.lset(undefined, state);
    qunit.propEqual(state, {app: {}}, "Reset DB - OK");

    state = loggedLen.cset([], true, state);
    qunit.propEqual(state, {app: {logged: true}}, "logged - OK");

    state = loggedLen.cset([], false, state);
    qunit.propEqual(state, {app: {logged: false}}, "logged - OK");
});
test('Session creation', () => {
    let sessionLen = api.buildContextLen("app.sessions.{:sessionID}.(user,logged)");
    let state = {};
    state = sessionLen.lset(undefined, state);
    qunit.propEqual(state, {app: {sessions: {}}}, "Reset DB - OK");

    state = sessionLen.cset(["s1"], ["user1", false], state);
    qunit.propEqual(state,
        {
            app: {
                sessions: {
                    "s1": {
                        "logged": false,
                        "user": "user1"
                    }
                }
            }
        }, "Session created - OK");

    state = sessionLen.cset(["s1"], ["user1", true], state);
    qunit.propEqual(state,
        {
            app: {
                sessions: {
                    "s1": {
                        "logged": true,
                        "user": "user1"
                    }
                }
            }
        }, "Session update - OK");


    state = sessionLen.cset(["s1"], undefined, state);
    qunit.propEqual(state,
        {
            app: {
                sessions: {
                    "s1": {}
                }
            }
        }, "Session created - OK");
    let sessionDeleteLen = sessionLen.spec.pointers["app.sessions.{:sessionID}"].bindContext(["s1"]);
    state = sessionDeleteLen.set(undefined, state);
    qunit.propEqual(state,
        {
            app: {
                sessions: {}
            }
        }, "Session deleted - OK");


});

test('Find specific values', () => {
    let usersLen = api.buildContextLen("app.users.cos.nested.more.{:uID}.(email,data.name,isActive)");
    let users = [
        ["u1", "admin@pmsoft.eu", "administrator", true],
        ["u2", "normal@pmsoft.eu", "user", true],
        ["u3", "inactive@pmsoft.eu", "inactive", false]
    ];
    let state = {};
    state = usersLen.lset(users, state);

    qunit.propEqual(usersLen.spec.contextMap, ["uID"], "contextMap - OK");
    qunit.propEqual(usersLen.spec.valueMap, ["email", "name", "isActive"], "valueMap - OK");

    let user = usersLen.find({
        email: "admin@pmsoft.eu",
        isActive: true
    }).on(state);
    qunit.propEqual(user, [
        ["u1", "admin@pmsoft.eu", "administrator", true]
    ], "find positive - OK");

    user = usersLen.find({
        email: "admin@pmsoft.eu",
        isActive: false
    }).on(state);
    qunit.propEqual(user, [], "find positive - OK");

    user = usersLen.find({
        isAdmistrator: {
            email: "admin@pmsoft.eu",
            isActive: true
        },
        isActive: false
    }).on(state);
    qunit.propEqual(user, [
        ["u1", "admin@pmsoft.eu", "administrator", true],
        ["u3", "inactive@pmsoft.eu", "inactive", false]
    ], "find with or condition positive - OK");
});
