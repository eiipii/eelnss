const example = {
    data: {
        type: "project",
        id: "root"
    },
    subtasks: [
        {
            data: {
                type: "group",
                name: "develop",
                id: "group1"
            },
            subtasks: [
                {
                    data: {
                        type: "task",
                        name: "setup",
                        id: "task1",
                        plan: [0, 3],
                        workEffort: 3,
                        tag: ["dev"],
                        assigned: ["dev1"]
                    }
                },
                {
                    data: {
                        type: "task",
                        name: "install",
                        id: "task2",
                        plan: [2, 7],
                        workEffort: 10,
                        tag: ["dev", "work"],
                        assigned: ["dev2"]
                    }
                },
                {

                    data: {
                        type: "task",
                        name: "deploy",
                        id: "task3",
                        plan: [6, 10],
                        workEffort: 3,
                        tag: ["doc"],
                        assigned: ["dev1"]
                    }
                },
                {
                    data: {
                        type: "group",
                        id: "group2",
                        name: "automate"
                    },
                    subtasks: [
                        {
                            data: {
                                type: "task",
                                name: "script",
                                id: "task4",
                                plan: [15, 20],
                                issue_id: ["is14"],
                                workEffort: 10,
                                tag: ["dev"],
                                assigned: ["dev1"]
                            }
                        },
                        {
                            data: {
                                type: "task",
                                name: "running",
                                id: "task5",
                                plan: [20, 30],
                                workEffort: 1,
                                tag: ["auto"],
                                assigned: ["dev4"]
                            }
                        }
                    ]
                }
            ]
        },
        {
            data: {
                type: "group",
                id: "group3",
                name: "support"
            },
            subtasks: [
                {
                    data: {
                        type: "task",
                        name: "sop",
                        id: "task6",
                        plan: [10, 14],
                        issue_id: ["is1"],
                        workEffort: 10,
                        tag: ["sop"],
                        assigned: ["dev1"]
                    }
                },
                {
                    data: {
                        type: "task",
                        name: "clean",
                        id: "task7",
                        plan: [20, 30],
                        issue_id: ["is10"],
                        workEffort: 1,
                        tag: ["sop"],
                        assigned: ["tester"]
                    }
                }
            ]
        }
    ]
}

const treeModel = {
    getLabel: tree => tree.data,
    getChildren: tree => (tree.subtasks || []),
    constructTree: (label, children) => ({
        data: label,
        subtasks: children
    })
};


export default {
    treeModel,
    example
}
