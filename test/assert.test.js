import assert from '../src/assert'

test('assert true is ok', () => {
    expect(assert(true, 'error message')).not.toBeDefined();
});

test('assert false toThrow', () => {
    expect(() => assert(false, 'error message')).toThrow('error message');
});