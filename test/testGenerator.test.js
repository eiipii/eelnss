import _ from 'lodash';
import testGenerator from "./utils/testGenerator";

test("some simple javascript constructions", () => {
    expect(_.keys({})).toStrictEqual([]);
    expect([1, 2, 3].slice(0, 2)).toStrictEqual([1, 2]);
    expect([1, 2, 3].slice(2)).toStrictEqual([3]);
    expect([1, 2, 3].slice(0, 1)).toStrictEqual([1]);
    expect([1, 2, 3].slice(1)).toStrictEqual([2, 3]);
    expect([1, 2, 3].slice(0, 0)).toStrictEqual([]);
});
test("keys generator generate unique keys", () => {
    expect(_.chain(_.range(1000)).map(testGenerator.keyNameGen).uniq().value().length).toBe(1000);
    expect(testGenerator.generateUniqueKeySet(30).length).toBe(30);

});
test("cartesian product", () => {
    let keys1 = testGenerator.generateUniqueKeySet(3);
    let keys2 = testGenerator.generateUniqueKeySet(2);
    let keys3 = testGenerator.generateUniqueKeySet(1);

    expect(testGenerator.cartesianProduct([])).toStrictEqual([
        []
    ]);
    expect(testGenerator.cartesianProduct([keys1])).toStrictEqual(_.map(keys1, function (k) {
        return [k];
    }));
    expect(testGenerator.cartesianProduct([keys1, keys2]).length).toStrictEqual(keys1.length * keys2.length);
    expect(testGenerator.cartesianProduct([keys1, keys2, keys3]).length).toStrictEqual(keys1.length * keys2.length * keys3.length);

    expect(testGenerator.cartesianProduct([
        ["a", "b"],
        ["x", "y"]
    ])).toStrictEqual([
        ["a", "x"],
        ["a", "y"],
        ["b", "x"],
        ["b", "y"]
    ]);
    expect(testGenerator.cartesianProduct([
        ["a", "b"],
        ["x", "y"],
        ["p"]
    ])).toStrictEqual([
        ["a", "x", "p"],
        ["a", "y", "p"],
        ["b", "x", "p"],
        ["b", "y", "p"]
    ]);
});
test("contextLensValuesGenerator zero values", () => {
    expect(_.range(0)).toStrictEqual([]);
    expect(testGenerator.generateUniqueKeySet(0)).toStrictEqual([]);
    expect(testGenerator.contextLensValuesGenerator(0, 1)).toStrictEqual([
        []
    ]);
    expect(testGenerator.contextLensValuesGenerator(0, 4)).toStrictEqual([
        []
    ]);
});
test("contextLensValuesGenerator size values", () => {
    let generatedValues220 = testGenerator.contextLensValuesGenerator(2, 2, 1);
    expect(generatedValues220).toHaveLength(2 ** 2);
    expect(generatedValues220[0]).toHaveLength(2 + 1);


    let generatedValues223 = testGenerator.contextLensValuesGenerator(2, 2, 3);
    expect(generatedValues223).toHaveLength(2 ** 2);
    expect(generatedValues223[0]).toHaveLength(2 + 3);

    let generatedValues253 = testGenerator.contextLensValuesGenerator(2, 5, 3);
    expect(generatedValues253).toHaveLength(2 ** 5);
    expect(generatedValues253[0]).toHaveLength(5 + 3);
});

test("contextLensValuesGenerator", () => {

    let keySizeXcontextSize = testGenerator.cartesianProduct([_.range(1, 5), _.range(1, 5)]);
    _.chain(keySizeXcontextSize).map(function (a_b) {
        let nrOfPossibleKeyValues = a_b[0];
        let listSize = a_b[1];
        let lenValues = testGenerator.contextLensValuesGenerator(nrOfPossibleKeyValues, listSize);
        expect(_.isArray(lenValues)).toBe(true);
        //, "list of values is array for (" + nrOfPossibleKeyValues + "," + listSize + ")");
        expect(lenValues.length).toBe(Math.pow(nrOfPossibleKeyValues, listSize));
        //, "len of the values is keySetSize^contextSize");
        _.forEach(lenValues, function (valueList) {
            expect(_.isArray(valueList)).toBeTruthy(); //, "each value is an array : " + valueList);
            expect(valueList.length).toBe(listSize);
            _.forEach(valueList, function (singleValue) {
                expect(_.isString(singleValue)).toBe(true);
            });
        });
    }).value();

});

test("mutateValuesOnContext", () => {
    let values = [
        ["a", "b", 1]
    ];
    let mutated = testGenerator.mutateValuesOnContext(values, 2, 1);
    expect(mutated[0][0]).toStrictEqual(values[0][0]);
    expect(mutated[0][1]).toStrictEqual(values[0][1]);
    expect(mutated[0][2]).not.toStrictEqual(values[0][2]);

});

