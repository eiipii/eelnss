import _ from "lodash"
import eelnss from "../index";

const monoid = ({zero, operation}) => ({
    zero,
    operation,
    fold: (mList) => _.reduce(mList, operation, zero())
});

const optionedMonoid = (monoidInstance) => monoid({
    zero: monoidInstance.zero,
    operation: (a, b) => {
        const aOpt = a ? a : monoidInstance.zero();
        const bOpt = b ? b : monoidInstance.zero();
        return monoidInstance.operation(aOpt, bOpt)
    }
})

// No semigroup assumed, reduce left always.
const magma = ({operation}) => ({operation})

function optionedMagmaMonoid(magma) {
    return monoid({
        zero: () => undefined,
        operation: (a, b) => {
            if (_.isUndefined(a)) {
                return b;
            }
            if (_.isUndefined(b)) {
                return a;
            }
            return magma.operation(a, b)
        }
    })
}

const intMonoid = monoid({
    zero: () => 0,
    operation: (a, b) => a + b
})
const intOptMonoid = optionedMonoid(intMonoid)

const listMonoid = monoid({
    zero: () => [],
    operation: (a, b) => [].concat(a, b)
})
const listOptMonoid = optionedMonoid(listMonoid)

const setOnArrayMonoid = monoid({
    zero: () => [],
    operation: (a, b) => _.union(a, b)
})
const setOnArrayOptMonoid = optionedMonoid(setOnArrayMonoid)

// [x1,x2] segments, with max operation. x1 <= x2
const timeSegmentMonoid = optionedMagmaMonoid(magma({
    operation: (a, b) => [Math.min(a[0], b[0]), Math.max(a[1], b[1])]
}))


function monoidAggregator({lift, monoid}) {
    return {
        lift,
        operation: monoid.operation,
        aggregate: monoid.fold,
    }
}


function crossProductMonoids(monoidList) {
    return monoid({
        zero: () => monoidList.map(m => m.zero()),
        operation: (a, b) => monoidList.map(
            (monoid, index) => monoid.operation(a[index], b[index])
        )
    })
}

function lenMappedMonoid(lenExpression, baseMonoid) {
    const aLen = eelnss.api.buildLen(lenExpression)
    return monoid({
        zero: () => aLen.set(baseMonoid.zero(), {}),
        operation: (a, b) => aLen.set(baseMonoid.operation(aLen.get(a), aLen.get(b)), {})
    })

}


function clenMappedMonoids(clenExpression, monoidsList) {
    let clen = eelnss.api.buildContextLen(clenExpression);
    if (clen.spec.contextSize != 0) {
        throw new Error("No context expected")
    }
    const monList = crossProductMonoids(monoidsList)
    return monoid({
        zero: () => clen.cset([], monList.zero(), {}),
        operation: (a, b) => {
            const aL = clen.cget([], a)
            const bL = clen.cget([], b)
            return clen.cset([], monList.operation(aL, bL), {})
        }
    })
}


function clenWithContextMonoids(clenExpression, monoidsList) {
    let clen = eelnss.api.buildContextLen(clenExpression);
    const monList = crossProductMonoids(monoidsList)
    const buildOnContext = (contextArray) => {
        return monoid({
            zero: () => clen.lset(contextArray.map(c => [].concat(c, monList.zero())), {}),
            operation: (a, b) => {
                const aL = clen.lget(a)
                const bL = clen.lget(b)
                const resultAsArray = contextArray.map((c, index) => {
                    const aSingle = aL[index].slice(c.length)
                    const bSingle = bL[index].slice(c.length)
                    return [].concat(c, monList.operation(aSingle, bSingle))
                })
                return clen.lset(resultAsArray, {});
            }
        });
    }
    return {
        onContext: (contextObject) => buildOnContext(clen.extract(contextObject)),
        onArrayContext: buildOnContext
    }
}

export default {
    constructors: {
        crossProductMonoids,
        monoid,
        magma,
        optionedMagmaMonoid
    },
    structures: {
        lenMappedMonoid,
        clenMappedMonoids,
        clenWithContextMonoids
    },
    monoids: {
        intMonoid,
        intOptMonoid,
        listMonoid,
        listOptMonoid,
        setOnArrayMonoid,
        setOnArrayOptMonoid,
        timeSegmentMonoid
    },
    monoidAggregator: monoidAggregator
}
