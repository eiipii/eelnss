import _ from 'lodash';

function andComposition(elements) {
    return function (state) {
        return _.every(elements, function (testSubFunction) {
            return testSubFunction(state);
        });
    };
}

function orComposition(elements) {
    return function (state) {
        return _.some(elements, function (testSubFunction) {
            return testSubFunction(state);
        });
    };
}

function buildBooleanCheckerForArrayValue(expectedValue, indexInArray) {
    return function (stateAsArray) {
        return stateAsArray[indexInArray] === expectedValue;
    };
}


// Build a Array => bool function.
// arrayNamesSpecification is a map {columnName -> indexInArray }
// queryObject encode a list of boolean criteria.
// AND case: each key is a columnName and value is not an object
// OR case: there is a key that contains a object.
function arrayBooleanFromQueryObject(arrayNamesSpecification, queryObject) {
    function _buildSingleCriteria(value, indexNumber) {
        if (_.isObject(value)) {
            return arrayBooleanFromQueryObject(arrayNamesSpecification, value);
        } else {
            return buildBooleanCheckerForArrayValue(value, indexNumber);
        }
    }

    const isOr = _.chain(queryObject).values().some(function (value) {
        return _.isObject(value);
    }).value();
    const singleCriteria = _.chain(queryObject).keys().map(function (key) {
        const value = queryObject[key];
        const indexValue = arrayNamesSpecification[key];
        return _buildSingleCriteria(value, indexValue);
    }).value();
    if (isOr) {
        return orComposition(singleCriteria);
    } else {
        return andComposition(singleCriteria);
    }
}

export default {
    buildCriteria: arrayBooleanFromQueryObject
};
