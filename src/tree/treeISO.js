import * as fp from "fp-rosetree";

const treeIsomorphism = ({sourceModel, targetModel}) => {
    return {
        transform: (tree) => fp.switchTreeDataStructure(sourceModel, targetModel, tree)
    }
}

const treeModelUtils = ({treeModel}) => ({
    flat: (tree) => {
        const accumulatorForFlat = {
            seed: [],
            visit: (result, traversalState, treeValue) => {
                result.push(treeModel.getLabel(treeValue));
                return result;
            }
        };
        return fp.preorderTraverseTree(treeModel, accumulatorForFlat, tree)
    },
    map: (tree, mapper) => {
        return fp.mapOverTree(treeModel, mapper, tree);
    }
})


export default {
    treeIsomorphism,
    treeModelUtils,
}
