import _ from 'lodash';
import utils from "./testGenerator";

import qunit from './qunitApiMapping'
// Laws checkers
// --------------

function checkLensesLaws(len, containerBuilder, v1, v2) {
    let zeroContainer = () => {
        return len.set(undefined, containerBuilder());
    };
    // set get law
    qunit.propEqual(len.get(len.set(v1, containerBuilder())), v1, " get(set(v,c)) == v for value " + JSON.stringify(v1));
    qunit.propEqual(len.get(len.set(v2, containerBuilder())), v2, " get(set(v,c)) == v for value " + JSON.stringify(v2));
    // get set law
    qunit.propEqual(len.set(len.get(zeroContainer()), zeroContainer()), zeroContainer(), " set(get(c),c) == c for zero container for values" + JSON.stringify(v1) + " , " + JSON.stringify(v2));
    let noEmptyContainer = len.set(v1, containerBuilder());
    qunit.propEqual(len.set(len.get(noEmptyContainer), noEmptyContainer), len.set(v1, containerBuilder()), " set(get(c),c) == c for noempty container ");

    // set set law
    qunit.propEqual(
        len.set(v2, len.set(v1, containerBuilder())),
        len.set(v2, containerBuilder()),
        " set(v2,set(v1,c)) == set(v2,c) on container c=" + JSON.stringify(containerBuilder()) + " for values: v1=" + JSON.stringify(v1) + ", v2=" + JSON.stringify(v2)
    );
}

function _checkContextLensesLaws(clen, containerBuilder, listOfValues) {
    let zeroContainer = () => {
        return clen.lset(undefined, containerBuilder());
    };
    // lset lget law
    qunit.propEqual(clen.lget(clen.lset(listOfValues, zeroContainer())), listOfValues, " lget(lset(v,c)) == v");
    // get set law
    qunit.propEqual(clen.lset(clen.lget(zeroContainer()), zeroContainer()), zeroContainer(), " set(get(c),c) == c for zero container ");
    let noEmptyContainer = clen.lset(listOfValues, containerBuilder());
    qunit.propEqual(clen.lset(clen.lget(noEmptyContainer), noEmptyContainer), clen.lset(listOfValues, containerBuilder()), " set(get(c),c) == c for noempty container ");

    // lset lset law
    // The context part must by identical
    let mutatedListOfValues = utils.mutateValuesOnContext(listOfValues, clen.spec.contextSize, clen.spec.valueSize);
    qunit.propEqual(
        clen.lset(mutatedListOfValues, clen.lset(listOfValues, containerBuilder())),
        clen.lset(mutatedListOfValues, containerBuilder()),
        " lset(v2,lset(v1,c)) == lset(v2,c) on "
    );
}

function _checkContextLenses(cLen, listOfValues) {

    let emptyContainerBuilder = () => {
        return {};
    };
    // check lens laws after on single binding
    _.forEach(listOfValues, function (singleValueSet) {
        let binding = cLen.bindContextValue(singleValueSet);
        let secondValue = utils.generateUniqueKeySet(cLen.spec.valueSize);
        checkLensesLaws(binding.len, emptyContainerBuilder, binding.value, secondValue);
    });
    _checkContextLensesLaws(cLen, emptyContainerBuilder, listOfValues);
}


function _testRange(cLen) {
    if (cLen.spec.contextSize === 0) {
        return [1];
    } else {
        return [1, 2, 10];
    }
}

function checkContextLenApi(cLen) {
    function checkExtractorElements() {
        qunit.ok(cLen, "cLen defined");
        qunit.ok(cLen.signature, "missing signature");
        qunit.ok(cLen.spec, "missing specification on cLen " + cLen.signature);
        qunit.ok(_.isString(cLen.spec.signature), "missing specification.signature on cLen " + cLen.signature);
        qunit.ok(_.isNumber(cLen.spec.contextSize), "missing specification.contextSize on cLen " + cLen.signature);
        qunit.ok(_.isNumber(cLen.spec.valueSize), "missing specification.valueSize on cLen " + cLen.signature);
        qunit.ok(_.isNumber(cLen.spec.size), "missing specification.size on cLen " + cLen.signature);
        qunit.ok(_.isFunction(cLen.bindContext), "missing bindContext on cLen " + cLen.signature);
        qunit.ok(_.isFunction(cLen.bindContextValue), "missing bindContextValue on cLen " + cLen.signature);
        qunit.strictEqual(cLen.spec.size, cLen.spec.valueSize + cLen.spec.contextSize, "sizes don't match " + cLen.signature);
    }

    checkExtractorElements();

    _.forEach(_testRange(cLen), function (nrOkKeys) {
        let values = utils.contextLensValuesGenerator(nrOkKeys, cLen.spec.contextSize, cLen.spec.valueSize);

        expect(values).toHaveLength(nrOkKeys ** cLen.spec.contextSize);

        _checkContextLenses(cLen, values);
    });
}


function checkContextLenEquivalenceOnValues(cLenA, cLenB, values) {
    // setting a value give the same result
    let emptyContainerBuilder = () => {
        return {};
    };

    //    For fast debugging use this expressions
    //    let a = cLenA.lset(values, emptyContainerBuilder());
    //    let b = cLenB.lget(cLenA.lset(values, emptyContainerBuilder()));
    //
    //    let c = cLenB.lset(values, emptyContainerBuilder());
    //    let d = cLenA.lget(cLenB.lset(values, emptyContainerBuilder()));

    qunit.propEqual(cLenA.lset(values, emptyContainerBuilder()), cLenB.lset(values, emptyContainerBuilder()), " A.lset(values,{}) == B.lset(values,{}) ");
    qunit.propEqual(cLenB.lget(cLenA.lset(values, emptyContainerBuilder())), cLenA.lget(cLenB.lset(values, emptyContainerBuilder())), " B.lget(A.lset(values,{})) == A.lget(B.lset(values,{})) ");

}

function checkContextLenEquivalence(cLenA, cLenB) {
    let checkSpecificationMatch = () => {
        qunit.strictEqual(cLenA.spec.contextSize, cLenB.spec.contextSize, "Context size match");
        qunit.strictEqual(cLenA.spec.valueSize, cLenB.spec.valueSize, "Value size match");
    };
    checkSpecificationMatch();
    _.forEach(_testRange(cLenA), function (nrOkKeys) {
        let values = utils.contextLensValuesGenerator(nrOkKeys, cLenA.spec.contextSize, cLenA.spec.valueSize);
        checkContextLenEquivalenceOnValues(cLenA, cLenB, values);
    });
}


function checkContextLensesCrossProducts(clenA, clenB, crossProduct) {
    // test ranges
    let testRanges = _.zip(_testRange(clenA), _testRange(clenB));
    let valuesForLen = function (howManyValues, len) {
        return utils.contextLensValuesGenerator(howManyValues, len.spec.contextSize, len.spec.valueSize);
    };
    let AxB = crossProduct;
    // create values for each len
    _.forEach(testRanges, function (ranges) {
        let Arange = ranges[0];
        let Brange = ranges[1];
        let Arows = valuesForLen(Arange, clenA);
        let Brows = valuesForLen(Brange, clenB);
        let emptyDb = {};
        let filledDatabase = clenB.lset(Brows, clenA.lset(Arows, emptyDb));
        // cLenses must be orthogonal in the graph representation
        qunit.propEqual(filledDatabase, clenA.lset(Arows, clenB.lset(Brows, emptyDb)));


        qunit.propEqual(Arows, clenA.lget(filledDatabase), "A.lget( AxB.lset ) === A.lget( A.lset ) for orthogonal context lenses");
        qunit.propEqual(Brows, clenB.lget(filledDatabase), "B.lget( AxB.lset ) === B.lget( B.lset ) for orthogonal context lenses");
        let extractedCrossProductValue = AxB.lget(filledDatabase);

        let expectedCrossProduct = _.chain(Arows).map(function (aRow) {
            return _.map(Brows, function (bRow) {
                let aContext = aRow.slice(0, clenA.spec.contextSize);
                let aValue = aRow.slice(clenA.spec.contextSize);
                let bContext = bRow.slice(0, clenB.spec.contextSize);
                let bValue = bRow.slice(clenB.spec.contextSize);
                return [].concat(aContext, bContext, aValue, bValue);
            });
        }).flatten(true).value();
        qunit.propEqual(extractedCrossProductValue, expectedCrossProduct, "AxB.lget( A.lset(Avalues, B.lset(Bvalues, empty))) is OK");
    });

}

export default {
    checkLensesLaws,
    checkContextLenEquivalence,
    checkContextLenApi,
    checkContextLensesCrossProducts
};