import api from '../src/api'
import contextLensesLaws from "./utils/contextLensesLaws";

test('a.b', () => {
    let lenAB = api.crossProduct(
        api.buildContextLen("a"),
        api.buildContextLen("b")
    );
    contextLensesLaws.checkContextLenApi(lenAB);
});


test('invalid arguments', () => {
    expect(() => api.crossProduct(
        api.buildContextLen("a")
    )).toThrow("Pass at least 2 clens to build a cross product")
});

test('a.b X x.{:id}.name', () => {
    let lenAB = api.crossProduct(
        api.buildContextLen("a.b"),
        api.buildContextLen("x.{:id}.name")
    );
    expect(lenAB.signature).toBe("a.b X x.{:id}.name");

    let extracted = lenAB.cget(["user1"], {
        a: {
            b: "a.b value"
        },
        x: {
            "user1": {
                name: "Pawel"
            }
        }
    });
    expect(extracted).toStrictEqual(["a.b value", "Pawel"]);
    const initialData = [
        ["key1", "a.b value1", "Pawel1"],
        ["key2", "a.b value2", "Pawel2"]
    ];
    const setInitial = lenAB.lset(initialData, {});
    expect(setInitial).toStrictEqual(
        {
            "a": {
                "b": "a.b value2",
            },
            "x": {
                "key1": {
                    "name": "Pawel1",
                },
                "key2": {
                    "name": "Pawel2",
                },
            },
        }
    )
    //This is not true, because path a.b overlap on value set
    //contextLensesLaws.checkContextLenApi(lenAB);

});
