const path = require('path');

module.exports = [
    'source-map'
].map(devtool => ({
    mode: 'development',
    entry: './src/index.js',
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'eelnss.js',
        library: 'eelnss',
        libraryTarget: 'umd',
    },
    devtool,
    externals: {
      lodash: {
        commonjs: 'lodash',
        commonjs2: 'lodash',
        amd: 'lodash',
        root: '_',
      },
      'fp-rosetree': {
        commonjs: 'fp-rosetree',
        commonjs2: 'fp-rosetree',
        amd: 'fp-rosetree',
        root: 'fp-rosetree',
      },
    },
}));
