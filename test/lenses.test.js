import lenses from '../src/lenses'
import contextLensesLaws from "./utils/contextLensesLaws";
import api from "../src/api";

test('id len', () => {
    contextLensesLaws.checkLensesLaws(lenses.idLen, () => {
    }, "a", "b");
});

test('simple', () => {
    const simple = api.buildLen("simple");
    contextLensesLaws.checkLensesLaws(simple, () => {
    }, "a", "b");
});

test('len for a.(x,y,z)', () => {
    const len = api.buildLen("a.(x,y,z)");
    contextLensesLaws.checkLensesLaws(len, () => {
    }, ["a", "b", "c"], ["x", "y", "z"]);
});

test('illegal len for x.().y', () => {
    expect(() => api.buildLen("x.().y")).toThrow("Pass at least 2 lenses a telescope composition")
});

test('illegal len for x.(p).y', () => {
    expect(() => api.buildLen("x.(p).y")).toThrow("Pass at least 2 lenses a telescope composition")
});

test('invalid use of api', () => {
    expect(() => api.buildLen("simple.{:x}.a")).toThrow("Trying to build a len with a context parameter??")
});

test('id.id len', () => {
    const id2 = lenses.idLen.andThen(lenses.idLen)
    contextLensesLaws.checkLensesLaws(id2, () => {
    }, "a", "b");
    expect(id2.get({x: 1})).toStrictEqual({x: 1})
    expect(id2.set({y: 2}, {x: 1})).toStrictEqual({y: 2})
});


test('x.y len', () => {
    const xy = lenses.fieldLen("x").andThen(lenses.fieldLenLeaf("y"))
    contextLensesLaws.checkLensesLaws(xy, () => {
    }, "a", "b");
    expect(xy.get({x: {y: 1}})).toBe(1)
    expect(xy.set(2, {x: {y: 1}})).toStrictEqual({x: {y: 2}})
});


test('nil len', () => {
    expect(lenses.nilLen.get({})).not.toBeDefined()
    expect(lenses.nilLen.set("b", {})).toStrictEqual({})
});
