import api from '../src/api'
import contextLensesLaws from "./utils/contextLensesLaws";
import qunit from './utils/qunitApiMapping'

test('simple', () => {
    const simple = api.buildContextLen("simple");
    contextLensesLaws.checkContextLenApi(simple);
});
test('simple.nested', () => {
    const simple = api.buildContextLen("simple.nested");
    contextLensesLaws.checkContextLenApi(simple);
});
test('(a,b,c,d,e)', () => {
    const simple = api.buildContextLen("(a,b,c,d,e)");
    contextLensesLaws.checkContextLenApi(simple);
});
test('nested.path.(a,b,c,d,e)', () => {
    const simple = api.buildContextLen("nested.path.(a,b,c,d,e)");
    contextLensesLaws.checkContextLenApi(simple);
});
test('{:mapId}', () => {
    const simple = api.buildContextLen("{:mapId}");
    contextLensesLaws.checkContextLenApi(simple);
});
test('simple.{:mapId}', () => {
    const simple = api.buildContextLen("simple.{:mapId}");
    contextLensesLaws.checkContextLenApi(simple);
});
test('simple.nested.{:mapId}', () => {
    const simple = api.buildContextLen("simple.nested.{:mapId}");
    contextLensesLaws.checkContextLenApi(simple);
});
test('simple.nested.{:mapId}.prop', () => {
    const simple = api.buildContextLen("simple.nested.{:mapId}.prop");
    contextLensesLaws.checkContextLenApi(simple);
});

test('simple.nested.{:mapId}.prop.{:submapId}', () => {
    const simple = api.buildContextLen("simple.nested.{:mapId}.prop.{:submapId}");
    contextLensesLaws.checkContextLenApi(simple);
});
test('a.{:mapId}.(a,b,c,d)', () => {
    const simple = api.buildContextLen("a.{:mapId}.(a,b,c,d)");
    contextLensesLaws.checkContextLenApi(simple);
});

test('a.{:mapId}.(a.b, x.z, complex.nested) mapped to person.{:cid}.(name, contact, c)', () => {
    const simple = api.buildContextLen("a.{:mapId}.(a.b, x.z, complex.nested)");

    const initialData = [
        ["key1", 1, 2, 3],
        ["key2", 1, 2, 3]
    ];
    const setInitial = simple.lset(initialData, {});

    qunit.propEqual(setInitial, {
        "a": {
            "key1": {
                "a": {
                    "b": 1
                },
                "complex": {
                    "nested": 3
                },
                "x": {
                    "z": 2
                }
            },
            "key2": {
                "a": {
                    "b": 1
                },
                "complex": {
                    "nested": 3
                },
                "x": {
                    "z": 2
                }
            }
        }
    }, "Set initial match");

    const tableData = simple.lget(setInitial);

    const target = api.buildContextLen("person.{:cid}.(name, contact, c)");
    const persons = target.lset(tableData, {});

    const extracted = target.lget(persons);

    qunit.propEqual(extracted, initialData, "Initial data mapped and extracted match");

});
