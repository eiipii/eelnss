import _ from 'lodash';
import assert from "./assert";

//LEN[A,B]
function defineLen(getter, setter, specification) {
    assert(_.has(specification, "signature"), "No signature in specification");
    assert(_.has(specification, "valueSize"), "No valueSize in specification");
    assert(_.has(specification, "valueMap"), "No valueMap in specification");
    const len = {
        spec: {
            signature: specification.signature,
            valueSize: specification.valueSize,
            valueMap: specification.valueMap
        }
    };
    //get: A => B
    len.get = getter;
    //set: (B,A) => A
    len.set = setter;
    len.mod = function (f, a) {
        return len.set(f(len.get(a)), a);
    };
    len.andThen = function (lenBC) {
        return andThenCompose(len, lenBC);
    };
    return len;
}


const nilLen = (function () {
    function nilGetter(a) {
        return void (0);
    }

    function nilSetter(b, a) {
        return a;
    }

    return defineLen(nilGetter, nilSetter, {
        signature: "nil",
        valueSize: 0,
        valueMap: []
    });
})();

const idLen = (function () {
    function idGetter(a) {
        return a;
    }

    function idSetter(newa, a) {
        return newa;
    }

    return defineLen(idGetter, idSetter, {
        signature: "ID",
        valueSize: 1,
        valueMap: ["ID"]
    });
})();

// Lenses left composition
// Len[A,B] ~> Len[B,C] => Len[A,C]
function andThenCompose(lenAB, lenBC) {
    if (lenAB === idLen) {
        return lenBC;
    }

    function andThenGetter(a) {
        return lenBC.get(lenAB.get(a));
    }

    function andThenSetter(c, a) {
        return lenAB.mod(function (b) {
            return lenBC.set(c, b);
        }, a);
    }

    const signature = lenAB.spec.signature + "." + lenBC.spec.signature;
    const valueSize = lenAB.spec.valueSize * lenBC.spec.valueSize;
    return defineLen(andThenGetter, andThenSetter, {
        signature: signature,
        valueSize: valueSize,
        valueMap: lenBC.spec.valueMap
    });
}

function _fieldLenTemplate(fieldName, fillEmpty) {
    function fieldGetter(a) {
        if (_.isUndefined(a)) {
            return void (0);
        }
        // Arrays are not valid fieldLen containers.
        assert(!_.isArray(a), "Arrays are not valid fieldLen containers. Field name:" + fieldName + ". Argument:" + a);
        const value = a[fieldName];
        // If options.fillEmpty===true, then a empty value is filled with a {} object. This is used for nested lens zeros.
        if (fillEmpty === true && _.isUndefined(value)) {
            return {};
        }
        return value;
    }

    function fieldSetter(b, a) {
        // Arrays are not valid fieldLen containers.
        assert(!_.isArray(a), "Arrays are not valid fieldLen containers. Field name:" + fieldName + ". Argument:" + a);
        if (_.isUndefined(b)) {
            return _.omit(a, fieldName);
        }
        const extendO = {};
        extendO[fieldName] = b;
        return _.chain(a).clone().extend(extendO).value();
    }

    return defineLen(fieldGetter, fieldSetter, {
        signature: fieldName,
        valueSize: 1,
        valueMap: [fieldName]
    });
}

function fieldLenLeaf(fieldName) {
    return _fieldLenTemplate(fieldName, false);
}

function fieldLen(fieldName) {
    return _fieldLenTemplate(fieldName, true);
}


function telescopeCompose() {
    assert(arguments.length > 1, "Pass at least 2 lenses a telescope composition");
    const listOfLens = Array.prototype.slice.call(arguments);
    const emptyValues = _.range(listOfLens.length).map(function () {
        return void (0);
    });

    function telescopeGetter(a) {
        return _.map(listOfLens, function (singleCLen) {
            return singleCLen.get(a);
        });
    }

    function telescopeSetter(b, a) {
        let values = b;
        if (_.isUndefined(values)) {
            values = emptyValues;
        }
        return _.reduce(_.zip(listOfLens, values), function (finalA, len_value) {
            const len = len_value[0];
            const rvalue = len_value[1];
            return len.set(rvalue, finalA);
        }, a);
    }

    const telescopeSignature = _.reduce(listOfLens, function (accum, len, index) {
        if (index === listOfLens.length - 1) {
            return accum + len.spec.signature;
        }
        return accum + len.spec.signature + ",";
    }, "(") + ")";
    const telescopeValueMap = _.chain(listOfLens).map(function (len) {
        return len.spec.valueMap;
    }).flatten(true).value();

    return defineLen(telescopeGetter, telescopeSetter, {
        signature: telescopeSignature,
        valueSize: listOfLens.length,
        valueMap: telescopeValueMap
    });
}

function nestedFieldBuilder(nestedFieldsExpression) {
    const listOfFields = nestedFieldsExpression.split(".");
    return _.chain(listOfFields)
        .map(function (fieldName, index) {
            const isLast = ((listOfFields.length - index - 1) === 0);
            if (isLast) {
                return fieldLenLeaf(fieldName);
            } else {
                return fieldLen(fieldName);
            }
        })
        .reduce(andThenCompose, idLen)
        .value();
}


export default {
    defineLen,
    nilLen,
    idLen,
    fieldLenLeaf,
    fieldLen,
    telescopeCompose,
    andThenCompose,
    nestedFieldBuilder,
};
