import _ from "lodash"
import foldableTreeModel from "../../src/tree/foldableTreeModel.js";
import algebraTypes from "../../src/tree/algebraTypes";
import projectTasksTreeModel from "./projectTasksTreeModel";
import treeISO from "../../src/tree/treeISO";

describe('Tree models isomorphism', () => {

    test('Group by in monoid aggregation', () => {
        const m = algebraTypes.monoids;
        const taskMonoid = algebraTypes.structures.clenMappedMonoids(
            "(plan,workEffort,issue_id,tag,assigned)",
            [
                m.timeSegmentMonoid,
                m.intOptMonoid,
                m.listOptMonoid,
                m.setOnArrayOptMonoid,
                m.setOnArrayOptMonoid
            ])

        const projectPlan = foldableTreeModel.mapToAggregatedTreeModel(
            {
                sourceModel: projectTasksTreeModel.treeModel,
                monoidAggregator: algebraTypes.monoidAggregator({
                    lift: c => c,
                    monoid: taskMonoid
                })
            }
        ).transform(projectTasksTreeModel.example);
        expect(projectPlan).toMatchSnapshot();

        let utils = treeISO.treeModelUtils({treeModel: foldableTreeModel.readAggregatedTreeModel});

        const projectAsList = utils.flat(projectPlan);
        const byType = _.groupBy(projectAsList, p => p.source.type)
        expect(byType).toMatchSnapshot();


        const byDeveloper = _.groupBy(projectAsList.filter(t => t.result.value.assigned), t => t.result.value.assigned[0])

        const devReport = _.mapValues(byDeveloper, devGroup =>
            taskMonoid.fold(devGroup.map(t => t.result.value))
        );
        expect(devReport).toMatchSnapshot();

    })


});
