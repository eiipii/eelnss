import treeISO from "./treeISO.js";

const aggregationTreeModel = (monoidAggregator) => ({
    constructTree: (label, children) => {
        const monoidValue = monoidAggregator.lift(label);
        const subtotal = monoidAggregator.aggregate(children.map(
            c => c.result.total
        ));
        return {
            source: label,
            result: {
                value: monoidValue,
                subtotal: subtotal,
                total: monoidAggregator.operation(monoidValue, subtotal),
            },
            children
        }
    }
});

const readAggregatedTreeModel = {
    getLabel: tree => ({
        source: tree.source,
        result: tree.result,
    }),
    getChildren: tree => (tree.children || []),
    constructTree: (label, children) => ({
        source: label.source,
        result: label.result,
        children
    })
};


const mapToAggregatedTreeModel = ({sourceModel, monoidAggregator}) => {
    return treeISO.treeIsomorphism({
        sourceModel: sourceModel,
        targetModel: aggregationTreeModel(monoidAggregator)
    })
}


export default {
    mapToAggregatedTreeModel,
    readAggregatedTreeModel
};
