const example = {
    data: {
        number: 1
    },
    children: [
        {data: {number: 1}, children: []},
        {data: {number: 2}, children: []},
        {data: {number: 4}},
        {data: {number: 6}},
        {data: {number: 9}},
        {data: {number: 2}},
        {data: {number: 7}, children: []},
        {
            data: {number: 0}, children: [
                {data: {number: 2}, children: []},
                {data: {number: 7}, children: []},
            ]
        },
        {data: {number: 3}, children: []},
        {data: {number: 1}, children: []},
        {
            data: {number: 9}, children: [
                {data: {number: 1}, children: []},
                {data: {number: 2}, children: []},
                {data: {number: 4}, children: []},
            ]
        },
    ]
}

const treeModel = {
    getLabel: tree => tree.data,
    getChildren: tree => (tree.children || []),
    constructTree: (label, children) => ({
        data: label,
        children
    })
};


export default {
    treeModel,
    example
}
