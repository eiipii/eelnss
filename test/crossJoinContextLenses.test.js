import api from '../src/api'
import contextLensesLaws from "./utils/contextLensesLaws";
import qunit from './utils/qunitApiMapping'

test('Use case: guest.{:personID}.(name,address.street,address.zipCode) X room.{:roomID}.number ', () => {
    let personClen = api.buildContextLen("guest.{:personID}.(name,address.street,address.zipCode)");
    let roomClen = api.buildContextLen("room.{:roomID}.number");
    let personXroom = api.crossProduct(personClen, roomClen);

    let guestTableData = [
        ["person1", "Pedro", "Str. One", "00-001"],
        ["person2", "Juan", "Str. Two", "00-002"]
    ];
    let roomsTableData = [
        ["room1", "100"],
        ["room2", "101"]
    ];
    let database = {};

    database = personClen.lset(guestTableData, database);
    database = roomClen.lset(roomsTableData, database);

    qunit.propEqual(database, {
        "guest": {
            "person1": {
                "address": {
                    "street": "Str. One",
                    "zipCode": "00-001"
                },
                "name": "Pedro"
            },
            "person2": {
                "address": {
                    "street": "Str. Two",
                    "zipCode": "00-002"
                },
                "name": "Juan"
            }
        },
        "room": {
            "room1": {
                "number": "100"
            },
            "room2": {
                "number": "101"
            }
        }
    }, "fill database ok");

    let dbCrossLoad = personXroom.lget(database);

    qunit.propEqual(dbCrossLoad, [
        ["person1", "room1", "Pedro", "Str. One", "00-001", "100"],
        ["person1", "room2", "Pedro", "Str. One", "00-001", "101"],
        ["person2", "room1", "Juan", "Str. Two", "00-002", "100"],
        ["person2", "room2", "Juan", "Str. Two", "00-002", "101"]
    ], "Load by cross product ok");

    let updateForCrossProduct = [
        ["person1", "room1", "Pawel", "ul. Jeden", "01-000", "100X"]
    ];

    database = personXroom.lset(updateForCrossProduct, database);
    qunit.propEqual(database, {
        "guest": {
            "person1": {
                "address": {
                    "street": "ul. Jeden",
                    "zipCode": "01-000"
                },
                "name": "Pawel"
            },
            "person2": {
                "address": {
                    "street": "Str. Two",
                    "zipCode": "00-002"
                },
                "name": "Juan"
            }
        },
        "room": {
            "room1": {
                "number": "100X"
            },
            "room2": {
                "number": "101"
            }
        }
    }, "update for cross product OK");

});

test('Check Context lenses properties for: guest.{:personID}.(name,address.street,address.zipCode) X room.{:roomID}.number', () => {
    let personClen = api.buildContextLen("guest.{:personID}.(name,address.street,address.zipCode)");
    let roomClen = api.buildContextLen("room.{:roomID}.number");

    let personXroom = api.crossProduct(personClen, roomClen);
    contextLensesLaws.checkContextLensesCrossProducts(personClen, roomClen, personXroom);

    let roomXperson = api.crossProduct(roomClen, personClen);
    contextLensesLaws.checkContextLensesCrossProducts(roomClen, personClen, roomXperson);

});

