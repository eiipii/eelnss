import _ from 'lodash';
import assert from "./assert";
import lenses from "./lenses";
import booleanFunctions from "./boolean";

function defineContextLen(contextGetter, contextSetter, contextExtractor, specification) {
    assert(_.has(specification, "contextSize"), "No contextSize in specification");
    assert(_.has(specification, "valueSize"), "No valueSize in specification");
    assert(_.has(specification, "signature"), "No signature in specification");
    assert(_.has(specification, "pointers"), "No pointers in specification");
    assert(_.has(specification, "contextMap"), "No contextMap in specification");
    assert(_.has(specification, "valueMap"), "No valueMap in specification");
    const opt = specification;
    const clen = {
        spec: {
            contextSize: opt.contextSize,
            valueSize: opt.valueSize,
            signature: opt.signature,
            pointers: opt.pointers,
            contextMap: opt.contextMap,
            valueMap: opt.valueMap,
            selfPointer: opt.selfPointer
        }
    };
    if (clen.spec.selfPointer
    ) {
        clen.spec.pointers[clen.spec.selfPointer] = clen;
    }
    clen.spec.size = clen.spec.contextSize + clen.spec.valueSize;
    clen.cget = contextGetter;
    clen.cset = contextSetter;
    clen.extract = contextExtractor;
    clen.cmod = function (context, f, a) {
        return clen.cset(context, f(clen.cget(context, a)), a);
    };
    if (clen.spec.contextSize === 0) {
        clen.lget = function (container) {
            let contextWithValue;
            if (clen.spec.valueSize === 1) {
                contextWithValue = [clen.cget([], container)];
            } else {
                contextWithValue = clen.cget([], container);
            }
            return [contextWithValue];
        };

    } else {
        clen.lget = function (container) {
            return _listContextGetter(clen, container);
        };
    }
    clen.lset = function (listContextValue, container) {
        if (_.isUndefined(listContextValue) || listContextValue.length === 0) {
            return clen.cset([], void (0), container);
        }
        return _listContextSetter(clen, listContextValue, container);
    };
    // The bind operation just fix the context parameter and return a len.
    clen.bindContext = function (context) {
        assert(context.length === clen.spec.contextSize, "context size error");
        const bindedGet = function (a) {
            return clen.cget(context, a);
        };
        const bindedSet = function (b, a) {
            return clen.cset(context, b, a);
        };
        return lenses.defineLen(bindedGet, bindedSet, {
            signature: clen.spec.signature,
            valueSize: clen.spec.valueSize,
            valueMap: clen.spec.valueMap
        });
    };
    clen.bindContextValue = function (contextAndValue) {
        assert(contextAndValue.length === clen.spec.size, "context size error");
        const _context = contextAndValue.slice(0, clen.spec.contextSize);
        const _value = contextAndValue.slice(clen.spec.contextSize);
        const _len = clen.bindContext(_context);
        return {
            len: _len,
            value: _value,
            context: _context
        };
    };
    clen.find = function (queryCriteria) {
        const arrayColumns = clen.spec.contextMap.concat(clen.spec.valueMap);
        const arraySpec = _.chain(arrayColumns).reduce(function (spec, colName, index) {
            spec[colName] = index;
            return spec;
        }, {}).value();
        const criteria = booleanFunctions.buildCriteria(arraySpec, queryCriteria);
        return {
            on: function (state) {
                //TODO use internal iterators and make a fast filter for nested lenses
                return _.chain(clen.lget(state)).filter(function (arrayValues) {
                    return criteria(arrayValues);
                }).value();
            }
        };
    };
    return clen;
}


function _listContextGetter(cLen, container) {
    return _.chain(cLen.extract(container)).map(function (context) {
        return context.concat(cLen.cget(context, container));
    }).value();
}

function _listContextSetter(cLen, listContextValue, container) {
    return _.chain(listContextValue)
        .reduce(function (currContainer, contextAndValue) {
            const context = contextAndValue.slice(0, cLen.spec.contextSize);
            const value = contextAndValue.slice(cLen.spec.contextSize);
            if (cLen.spec.valueSize === 1) {
                return cLen.cset(context, value[0], currContainer);
            }
            return cLen.cset(context, value, currContainer);
        }, container).value();
}

function contextLenFromLen(len) {
    function cgetFromLen(context, a) {
        return len.get(a);
    }

    function csetFromLen(context, b, a) {
        return len.set(b, a);
    }

    return defineContextLen(cgetFromLen, csetFromLen, (a) => [[]], {
            contextSize: 0,
            valueSize: len.spec.valueSize,
            signature: len.spec.signature,
            pointers: {},
            contextMap: [],
            valueMap: len.spec.valueMap,
            selfPointer: len.spec.signature
        }
    );
}

const idContextLen = contextLenFromLen(lenses.idLen);

function _mapContextLenTemplate(fillEmpty) {

    function cgetMap(context, a) {
        assert(_.isArray(context), "context for map must be a single value");
        if (fillEmpty && context.length === 0) {
            return {};
        }
        assert(context.length === 1, "context for map must be a single value");
        const key = context[0];
        const value = a[key];
        if (fillEmpty && _.isUndefined(value)) {
            return {};
        }
        return value;
    }

    function csetMap(context, b, a) {
        assert(_.isArray(context), "context for map must be a single value");
        if (context.length === 0) {
            return a;
        }
        assert(context.length === 1, "context for map must be a single value");
        const key = context[0];
        if (_.isUndefined(b)) {
            return _.omit(a, key);
        }
        const extendO = {};
        extendO[key] = b;
        return _.chain(a).clone().extend(extendO).value();
    }

    function contextExtractorMap(a) {
        const keys = _.keys(a);
        if (keys.length === 0) {
            return [];
        }
        return _.chain(keys).map(function (key) {
            return [key];
        }).value();
    }

    return defineContextLen(cgetMap, csetMap, contextExtractorMap, {
            contextSize: 1,
            valueSize: 1,
            signature: "{:map}",
            pointers: {},
            contextMap: ["map"],
            valueMap: ["_"],
            selfPointer: undefined
        }
    );
}

const mapContextLenLeaf = _mapContextLenTemplate(false);
const mapContextLen = _mapContextLenTemplate(true);

function contextLenComposition(cLenAB, cLenBC) {
    if (cLenAB === idContextLen) {
        return cLenBC;
    }
    // if (cLenBC === idContextLen) {
    //     return cLenAB;
    // }
    const cAndThenSignature = cLenAB.spec.signature + "." + cLenBC.spec.signature;
    const cAndThenContextSize = cLenAB.spec.contextSize + cLenBC.spec.contextSize;
    const cAndThenValueSize = cLenAB.spec.valueSize * cLenBC.spec.valueSize;
    const cAndThenPartialPoints = _.extend(cLenAB.spec.pointers, cLenBC.spec.pointers);
    const selfPointer = cLenAB.spec.selfPointer + '.' + cLenBC.spec.selfPointer;
    const cAndThenSpecification = {
        contextSize: cAndThenContextSize,
        valueSize: cAndThenValueSize,
        signature: cAndThenSignature,
        pointers: cAndThenPartialPoints,
        contextMap: cLenAB.spec.contextMap.concat(cLenBC.spec.contextMap),
        valueMap: cLenBC.spec.valueMap,
        selfPointer: selfPointer
    };

    if (cLenAB.spec.contextSize === 0) {
        return simpleAndThen();
    }
    if (cLenBC.spec.contextSize === 0) {
        return simpleInverseAndThen();
    }
    return realCAndThen();

    function simpleAndThen() {
        const simpleCget = function (context, a) {
            return cLenBC.cget(context, cLenAB.cget([], a));
        };
        const simpleCset = function (context, c, a) {
            return cLenAB.cmod([], function (b) {
                return cLenBC.cset(context, c, b);
            }, a);
        };
        const simpleExtract = function (a) {
            return cLenBC.extract(cLenAB.cget([], a));
        };
        return defineContextLen(simpleCget, simpleCset, simpleExtract, cAndThenSpecification);
    }

    function simpleInverseAndThen() {
        const simpleCget = function (context, a) {
            return cLenBC.cget([], cLenAB.cget(context, a));
        };
        const simpleCset = function (context, c, a) {
            return cLenAB.cmod(context, function (b) {
                return cLenBC.cset([], c, b);
            }, a);
        };
        return defineContextLen(simpleCget, simpleCset, cLenAB.extract, cAndThenSpecification);
    }

    function realCAndThen() {
        const composedCget = function (context, a) {
            const ABcontext = context.slice(0, cLenAB.spec.contextSize);
            const BCcontext = context.slice(cLenAB.spec.contextSize);
            return cLenBC.cget(BCcontext, cLenAB.cget(ABcontext, a));
        };
        const composedCset = function (context, c, a) {
            const ABcontext = context.slice(0, cLenAB.spec.contextSize);
            const BCcontext = context.slice(cLenAB.spec.contextSize);
            return cLenAB.cmod(ABcontext, function (b) {
                return cLenBC.cset(BCcontext, c, b);
            }, a);
        };
        const composedExtactor = function (a) {
            return _.chain(cLenAB.extract(a)).map(function (single_b_context) {
                const b = cLenAB.cget(single_b_context, a);
                return _.map(cLenBC.extract(b), function (single_c_context) {
                    return single_b_context.concat(single_c_context);
                });
            }).flatten(true).value();
        };
        return defineContextLen(composedCget, composedCset, composedExtactor, cAndThenSpecification);
    }
}


export default {
    defineContextLen,
    idContextLen,
    mapContextLenLeaf,
    mapContextLen,
    contextLenFromLen,
    contextLenComposition,
};
