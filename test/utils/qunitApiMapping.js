function ok(value, message) {
    expect(value).toBeDefined()
}

function propEqual(v1, v2, message) {
    expect({
        msg: message,
        value: v1
    }).toStrictEqual({
        msg: message,
        value: v2
    })
}

function strictEqual(v1, v2, message) {
    expect(v1).toStrictEqual(v2)
}

function equal(v1, v2, message) {
    expect(v1).toBe(v2)
}

export default {
    ok,
    propEqual,
    strictEqual,
    equal
}
