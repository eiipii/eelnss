import _ from "lodash"
import dataLabelTreeModel from "./dataLabelTreeModel";
import foldableTreeModel from "../../src/tree/foldableTreeModel.js";
import algebraTypes from "../../src/tree/algebraTypes";
import treeISO from "../../src/tree/treeISO";

describe('Tree models isomorphism', () => {

    test('Create aggregated tree model ', () => {
        const aggregatedTree = foldableTreeModel.mapToAggregatedTreeModel(
            {
                sourceModel: dataLabelTreeModel.treeModel,
                monoidAggregator: algebraTypes.monoidAggregator({
                    lift: a => a.number,
                    monoid: algebraTypes.monoids.intMonoid
                })
            }
        ).transform(dataLabelTreeModel.example);
        expect(aggregatedTree).toMatchSnapshot();

        let treeUtils = treeISO.treeModelUtils({treeModel: foldableTreeModel.readAggregatedTreeModel});
        let aggregatedFlatten = treeUtils.flat(aggregatedTree);
        expect(aggregatedFlatten).toMatchSnapshot();

        const withSummary = treeUtils.map(aggregatedTree, (label) => {
            const mapped = _.clone(label)
            mapped.result.summary = "Value is " + label.result.value + " and total is " + label.result.total;
            return mapped;
        })
        expect(withSummary).toMatchSnapshot();
    });

    test('Aggregated model tree is children safe', () => {
        let treeUtils = treeISO.treeModelUtils({treeModel: foldableTreeModel.readAggregatedTreeModel});
        //No children, so should default to []
        const mapped = treeUtils.map({
            source: {},
            result: {}
        }, (label) => {
            const mapped = _.clone(label)
            mapped.result.ok = true;
            return mapped;
        });
        expect(mapped).toStrictEqual({
            source: {},
            result: {
                ok: true
            },
            children: []
        });
    })


});
